<?php
wp_head();

if (isset($_GET['filter'])){
        if($_GET['search'] && !empty($_GET['search']))
        {
            $search = $_GET['search'];
        } else {
            $search = "";
        }

        if($_GET['subject'] && !empty($_GET['subject']))
        {
            $subject = $_GET['subject'];
        }

        if($_GET['standard'] && !empty($_GET['standard']))
        {
            $standard = $_GET['standard'];
        }
    }else{
        // $search ="";
        // $subject =array('accounts','maths','english');
        // $standard =array(11,12);
    }
?>
<div class="learndash-course-grid">
	
       <!-- <div class="filter"> -->
            <form method="GET" action="<?php the_permalink(); ?>" name="filter" class="filter">
                <div class="form">
                    <div class="search">
                        <label for="search">Keyword</label>
                        <input type="text" name="search" id="search">
                    </div>
                    <?php 
                    if ( get_query_var( 'paged' ) ) {
                        $paged = get_query_var( 'paged' );
                     } else if ( get_query_var( 'page' ) ) {
                        // This will occur if on front page.
                        $paged = get_query_var( 'page' );
                     } else {
                         $paged = 1;
                     }
                        $terms_sub = get_terms( array(
                          'taxonomy' => 'subject',
                          'hide_empty' => true,
                        ) );
                        $terms = get_terms( array(
                          'taxonomy' => 'standard',
                          // 'hide_empty' => true,
                        ) );
                    ?>
                    <div class="taxonomy">
                        <select name="subject" id="subject" class="terms">
                            <option selected>All Subjects</option>
                            <?php foreach ($terms_sub as $term_sub) :?>
                                <option value="<?php echo $term_sub->slug;?>"><?php echo $term_sub->name;?></option>
                             <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="taxonomy">
                        <select name="standard" id="standard" class="terms">
                            <option selected>All Grade</option>
                            <?php foreach ($terms as $term) : ?>
                                <option value="<?php echo $term->slug;?>"><?php echo $term->name;?></option>
                             <?php endforeach; ?> 
                             <!-- <option value="11">11</option>
                              <option value="12">12</option> -->
                        </select>
                    </div>
                </div>
                <div class="submit">
                    <button type="submit">Apply</button>
                </div> 
                
            </form>
        <!-- </div> -->
        <div class="items-wrapper">
        <?php
        if (isset($_GET['filter'])){
                $wpnew=array(
                    'post_type'=>'sfwd-courses',
                    'post_status'=>'publish',
                    'post_per_page' => 6,
                    'paged'          => $paged, 
                    'tax_query'     => array(
                        array(
                        'taxonomy'  => 'subject',
                        'field'     => 'slug',
                        'terms'     => $subject
                        ),
                        array(
                            'taxonomy'  => 'standard',
                            'field'     => 'slug',
                            'terms'     => $standard
                        )
                    )
                    // 'meta_query' => array(
                    //     array(
                    //         'key' => 'title',
                    //         'value' => $search,
                    //         'compare' => 'LIKE'
                    //     ),
                    // )

                );
            }else{
                $wpnew=array(
                    'post_type'=>'sfwd-courses',
                    'post_status'=>'publish',
                    'post_per_page' => -1,
                    'paged' => $paged, 
                );
            }
               $the_query = new WP_Query( $wpnew );
 
                // The Loop
                if( $the_query->have_posts()){
                    
                    while ( $the_query->have_posts() ) {
                        $the_query->the_post();
                       /* $the_query->the_post();
                        $imagepath=wp_get_attachment_image_src(get_post_thumbnail_id(),'large');*/
                        ?>
                        
                            <div class="post">
                                <!-- <div class="thumbnail"> 
                                    <img src=<?php echo $imagepath;?> />
                                </div>  -->
                                <div class="content">
                                    <div class="entry-title">
                                    <?php
                                        
                                        echo '<a>' . get_the_title() . '</a>';
                                    
                                    ?>
                                    </div>
                                </div>
                                <div class="button">
                                    <button type="submit"><a>Enroll</a></button>
                                </div>
                            </div>
                
                        
                <?php 
                    }
    
                }/* Restore original Post Data */
                ?>
                <div class = "news-list-pagination" >
 
<?php
 
$total_pages = $the_query -> max_num_pages;
 
if ($total_pages > 1) {
 
    $current_page = max(1, get_query_var('paged'));
 
    echo paginate_links(array(
        'base' => get_pagenum_link(1).
        '%_%',
        //'format' => '/page/%#%',
        'current' => $current_page,
        'total' => $total_pages,
        'prev_text' => __('<i class="fas fa-chevron-left page-prev"></i>'),
        'next_text' => __('<i class="fas fa-chevron-right page-next"></i>'),
    ));
}else {?>
    <h3> <?php _e('404 Error&#58; Not Found', ''); ?> </h3> <?php }
    wp_reset_postdata(); ?>
 
</div>
                <?php wp_reset_query();
                

             ?>
        </div>
    </div>

    
